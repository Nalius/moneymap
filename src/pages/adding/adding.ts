import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Transaction } from '../../database'
import { GeolocationService } from '../../services/geolocation.service';
/*
  Generated class for the Adding page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-adding',
  templateUrl: 'adding.html'
})
export class AddingPage {

	model : Transaction;

  constructor(public navCtrl: NavController, public navParams: NavParams ,public geo : GeolocationService) {
  	this.model = new Transaction(null,"");
  }

  ionViewDidLoad() {
    this.geo.get().then((resultado)=> {
        console.log(resultado);
    }).catch((e)=>{console.log(e);});
  }

  save(){
  	this.model.save().then(resultado => {
  		this.model = new Transaction(null,"");	
  		this.navCtrl.pop();
  	});
  }
}
